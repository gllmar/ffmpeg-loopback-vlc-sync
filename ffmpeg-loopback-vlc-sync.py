import requests
import time
import argparse
import os
import subprocess
import datetime

# Global variables for FFmpeg process management
ffmpeg_process = None
ffmpeg_start_datetime = None
initial_ffmpeg_position = 0
current_ffmpeg_filename = None  # Track the filename currently being processed by FFmpeg

def parse_arguments():
    parser = argparse.ArgumentParser(description='Synchronize VLC playback with FFmpeg and a loopback device.')
    parser.add_argument('--password', required=True, help="VLC's web interface password.")
    parser.add_argument('--port', type=int, default=8080, help="VLC's web interface port.")
    parser.add_argument('--url', default='localhost', help="VLC's web interface URL.")
    parser.add_argument('--interval', type=int, default=1, help='Status check interval in seconds.')
    parser.add_argument('--tolerance', type=float, default=5, help='Time tolerance for resynchronization in seconds.')
    parser.add_argument('--directory', default='~/Videos', help='Directory to search for media files.')
    parser.add_argument('--loopback-device', default='/dev/video0', help='Loopback device for video output.')
    parser.add_argument('--time-offset', type=float, default=0, help='Time offset for FFmpeg start time adjustment.')
    parser.add_argument('--freerun', action='store_true', help='Enable freerun mode to skip synchronization.')
    return parser.parse_args()

def query_vlc_status(args):
    url = f"http://{args.url}:{args.port}/requests/status.json"
    try:
        response = requests.get(url, auth=('', args.password))
        response.raise_for_status()
        return response.json()
    except requests.RequestException as e:
        print(f"Failed to query VLC status: {e}")
        return None

def find_matching_file(directory, filename):
    base_filename = os.path.splitext(filename)[0]
    for root, dirs, files in os.walk(os.path.expanduser(directory)):
        for file in files:
            if file.startswith(base_filename) and not file.startswith('.'):
                return os.path.join(root, file)
    return None

def calculate_current_playback_time():
    global ffmpeg_start_datetime, initial_ffmpeg_position
    if ffmpeg_start_datetime:
        elapsed_time = (datetime.datetime.now() - ffmpeg_start_datetime).total_seconds()
        return initial_ffmpeg_position + elapsed_time
    return 0

def resynchronize_media(file_path, start_time, args, force_freerun=False):
    global ffmpeg_process, ffmpeg_start_datetime, initial_ffmpeg_position, current_ffmpeg_filename
    video_changed = current_ffmpeg_filename != file_path
    should_restart = video_changed or (force_freerun and ffmpeg_process is None) or \
                     (not force_freerun and (abs(start_time - calculate_current_playback_time()) > args.tolerance))

    if should_restart:
        if ffmpeg_process:
            ffmpeg_process.terminate()
            ffmpeg_process.wait()

        current_ffmpeg_filename = file_path
        freerun = force_freerun or args.freerun

        if freerun:
            cmd = ['ffmpeg', '-loglevel', 'quiet', '-stream_loop', '-1', '-re', '-i', file_path, '-f', 'v4l2', args.loopback_device]
        else:
            loop_start_time = max(0, start_time + args.time_offset)
            cmd = ['ffmpeg', '-loglevel', 'quiet', '-stream_loop', '-1', '-re', '-ss', str(loop_start_time), '-i', file_path, '-f', 'v4l2', args.loopback_device]
        
        ffmpeg_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ffmpeg_start_datetime = datetime.datetime.now() if not freerun else None
        initial_ffmpeg_position = start_time if not freerun else 0

        print(f"{'Starting' if freerun else 'Resynchronizing'} media: {file_path} at position: {start_time}s" if not freerun else f"Starting media in freerun mode: {file_path}")
def main():
    args = parse_arguments()
    
    while True:
        status = query_vlc_status(args)
        if status and status.get('state') == 'playing':
            filename = status['information']['category']['meta'].get('filename')
            file_path = find_matching_file(args.directory, filename)
            if file_path:
                force_freerun = 'freerun' in file_path or args.freerun
                if force_freerun and current_ffmpeg_filename != file_path:
                    print("Freerun mode activated due to 'freerun' in the filename.")
                position = 0 if force_freerun else status.get('position') * status.get('length')
                resynchronize_media(file_path, position, args, force_freerun=force_freerun)
        else:
            print("Waiting for VLC to play media...")
        time.sleep(args.interval)

if __name__ == "__main__":
    main()
