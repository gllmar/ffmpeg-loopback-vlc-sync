#!/bin/bash

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
cd $SCRIPT_DIR
python ffmpeg-loopback-vlc-sync.py --freerun --password framboise --port 9990 --directory ~/Videos/loops/sync/ --loopback-device /dev/video61